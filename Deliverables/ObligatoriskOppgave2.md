# Oblig 2 – A-Game

* Team: A-Team (Gruppe 4): Oscar Nysten, Audun Haugen, Marius Aasebø, Peder Spooren

## Deloppgave 1:

### Referat av teammøter:

#### 17.03.2022
Tilstede: Oscar Nysten, Audun Haugen, Peder Spooren

Vi diskuterte litt om det med kollisjon, og hvordan dette skal gjøres i forhold til Tiled + kode, HUD. Har diskutert om vi skulle droppe multiplayer eller ikke, men vi bestemte oss for å iallefall legge til multiplayer på samme maskin, da dette virker trivielt å gjøre. Vi har også diskutert om det med å porte til Android/Web, og dette har vi bestemt oss for å gjøre mot slutten av prosjektet, om vi føler det passer med tid.

Vi ble enige om å fikse en bug i HUD-en, finne ut av kollisjon av objekter/bakken, prøve å finne ut av tyngdekraft.


#### 22.03.2022
Tilstede: Oscar Nysten, Audun Haugen, Peder Spooren, Marius Aasebø

Fram til dette møtet ble en HUD-bug fikset, fortsettet arbeidet med kollisjon og gravitasjon, noe som vi sliter litt med for tiden. Vi diskuterte fram en plan for hvordan vi skulle fikse kollisjon og gravitasjon, da vi tenker å gjøre om vår Player-klasse fra bunnen av, da vi hadde funnet en mulig løsning til de to egenskapene. Vi ble også enige om fremditige møter og hva som skulle gjøres fram til neste gang (finne ut av ting som omhandler det vi sliter med). 


#### 24.03.2022
Tilstede: Oscar Nysten, Peder Spooren

Utforsket mulige løsninger for problemene nevnt i forrige referat. Her ble kollisjon og bevegelse/gravitasjon fikset til en akseptabel implementasjon som funker. Vi har også småfikset på interfaces basert på denne implementasjonen, i tillegg til å fikse på enkelte ting ut i fra tilbakemeldingen vi fikk fra obligatorisk 1. Vi slet en del med å finne ut av bevegelse/gravitasjon, da bevegelsen fungerte som å stå på is (ingen "friksjon") og gravitasjonen ikke fungerte i det hele tatt i første omgang. Etter å ha søkt litt på nettet fant vi ut av løsninger til disse problemene som ga oss en grei implementasjon for disse funksjonene. Vi diskuterte videre hvordan vi skulle jobbe i morgen da det er frist for obligatorisk 2, hvor vi ble enige om at vi skal jobbe sammen med dokumentasjon som er påkrevd til den aktuelle obligatoriske oppgaven. Vi ble også enige om hva som skal gjøres til neste gang, som var å utforske akselerasjon av spiller og utvidelse av kartet, out-of-bounds detection og diverse rydding av kode. 


#### 25.03.2022
Tilstede: Oscar Nysten, Peder Spooren, Marius Aasebø

Planen for dette møtet var å gjøre ferdig alt av dokumentasjon og enkel rydding fram til deadline obligatorisk 2. Her ble det mye felles diskutering av de punktene som står i obligatorisk 2-4 i tillegg til diskusjon om hva som skulle være med i siste release, som tok mesteparten av tiden. Vi diskuterte også videre smått hva vi tenker å prioritere etter denne deadlinen, som vi ble enige om er fiender, poengsystem og mer fokus på tester. Vi gikk også gjennom tilbakemeldingen vi fikk fra obligatorisk 1, hvor vi fikset på det som kunne fikses. Til neste gang ble vi enige om å utforske elementer som fiender, og utbrede tester, i tillegg til klassediagram (som skal være klart til deadline) og å utforske poengsystemet videre. 


### Oppsummering (for punktene under referat)
Rollene i teamet trenger en liten oppdatering så vi gjør følgende endringer:

Produktansvarlig / Grafikkansvarlig 	- Marius <br>
Physicsansvarlig / kvalitetskontroll	- Oscar <br>
Dokumentasjonsansvarlig / Scrum master   - Peder <br>
Git-expert / Kundekontakt 				- Audun <br>

De første rollene i obligatorisk 1 var mer som placeholders roller, da vi følte etterhvert når vi startet prosjektet at rollene ville komme mer naturlig frem til oss etterhvert som vi jobber, enn om vi tvinger på roller gruppemedlemmer eventuelt/etterhvert ikke vil ha. Dette har vi funnet ut funket bra for oss. 

#### Rollenes betydning:
- Produktansvarlig er personen som har ansvaret til å definere hvordan spillet skal være og se ut. Dette inkluderer også å gjennomgå brukerhistorier, i tillegg til å komme med diverse idéer.

- Grafikkansvarlig er ansvarlig for alt av det grafiske i spillet. Dette gjelder utseende av spillkarakter/spillbrett, menyer og diverse andre "skjermer" (loading screen, game-over screen).

- Physicsansvarlig er ansvarlig for alt om fysikk og movement av spillet og andre objekter i spillet. Dette inkluderer da tyngdekraft, kollisjon og alt av movement til spilleren. 

- Kvalitetskontroll er personen som forsikrer at koden ser ryddig og pen ut, i tillegg til at koden fungerer som den skal i spillet når man kjører det. Her kan man tenke at tester skal gjøre mye av jobben automatisk, men kvalitetskontrolløren skal overse leveransen og de manuelle testene. 

- Dokumentasjonsansvarlig er ansvarlig for alt av dokumentasjon i prosjektet. Denne personen skal ikke skrive dokumentasjonen til metodene/klassene andre personer skriver, men denne personen skal kontrollere om det ser pent, riktig og forståelig ut.

- Scrum master er personen som styrer teamet, alt fra å dirigere møter og holde prosjektet i gang. Denne personen er altså den som tar initiativ, og om det er noe uklart eller uenigheter, så er det scrum master som har veto.

- Git-experten skal ha kontroll over alt som skjer i git og repositoriet. Dette gjelder pushing, pulling, merging og leveransekontroll (sammen med kvalitetskontrollør) for å nevne noe. 

- Kundekontakt er den personen som har kontakt med kunde, enten om det er kunden som har problem, eller om det er vi som etterspør tilbakemelding. 

Disse nye rollene har fungert veldig greit så langt, da disse rollene kom fram naturlig mens vi har jobbet med prosjektet. Her kan vi nevne at alle sammen er med på å implementerer metoder/klasser/tester, så det ikke bare er en del av gruppen som jobber med selve kodingen/denne rollen.


#### Prosjektmetodikk
Som nevnt i tilbakemeldingen fra obligatorisk 1, så gir det mening at scrum egner seg bedre. Dette fordi emnet er satt opp som sprint/milepæl modell. I tillegg ønsker vi å bruke Trello, som nevnt tidligere, noe som også kan bli brukt under scrum-metodikken. Nå igjen nevner jeg også punktet om fleksibilitet, som også scrum har noe av. Vi valgte i første omgang kanban, noe som ble litt dårlig forklart fram, men som vi også tror at vi var litt usikre på. Men siden emnet var satt opp slikt til å ha en sprint/milepæl modell, gitt vi naturlig over til en scrum-lignende metodikk. 

- Vi lagde en liten og enkel plan på starten av prosjektet slik at vi kunne få kommet i gang. 

- Da, ved hjelp av de tilgitte MVP-ene (og de vi prioritierte), bygde vi opp den første iterasjonen av spillet vårt. 

- Vi lagde tester til denne første iterasjonen (men som ble endret/fjernet pga. endringer gjort i senere stadiet av spillet), i tillegg til å teste spillet manuelt ved å kjøre det. 

- Nærme deadline, bestemte vi oss for hva som skulle bli sendt til kunde gjennom vurdering av prosjektet.

Dette ble da vår sprint 1, og prosessen blir da repetert i flere sprints, som tilsvarer den sprint modellen scrum har. Altså, obligatorisk 1 er sprint 1, obligatorisk 2 er sprint 2 osv. 

Vi skal lage en backlog i form av brukerhistorier, hvor vi oppdaterer obligatorisk 1 sin liste av brukerhistorier med en ny prioritert liste med disse hvor de fullførte er fjernet og eventuelle nye er lagt til. Vi vil også fortsette å bruke Trello for å holde oss organisert, hvor vi skal bli bedre på å konstruere kortene, da dette ble nevnt i tilbakemeldingen for obligatorisk 1.


#### Gruppedynamikk
Gruppedynamikken har fungert ganske bra så langt. Vi har ikke har noen store uenigheter, og de enkelte småe vi har hatt løste seg veldig fort gjennom hurtig diskusjon. Mye av arbeidet har blitt gjort i fellesskap, så vi har blitt enige og diskutert om alt. 

Kommunikasjonen har fungert bra. Ingen av oss har ingen problem med å ta ordet om det er noe man lurer på, og vi tenker så og si i lik bane så kommunikasjonen har gått naturlig fram så langt.


#### Retrospektiv
Vi har jobbet ganske bra sammen til nå, og vil fortsette slik fremover, hvis ikke enda bedre om mulig. Vi fokuserer videre på arbeidsvurdering slik at ting ikke overlapper, kommunikasjon i forhold til merging (accepting) slik at det ikke blir noen conflicts (noe som var et problem i starten av obligatorisk 1). Vi fortsetter også å fokusere videre på arbeid i fellesskap slik vi har gjort det så langt, hvor vi da gir oss selv "hjemmelekser" av ting som skal jobbes til neste møte, da det er dette som har fungert godt for oss. Alt av dette kan bli bedre, noe som vi prøver å strekke oss etter, som er grunnen til at vi fokuserer på disse områdene fremover.


Med tanke på hvem som committer og hvor mange ganger, så blir mesteparten av arbeidet gjort i fellesskap, noe som da kan være grunnen til at en person eller flere personer committer oftere enn andre. Her prøver vi da å veksle mellom hvem som har kontroll av kodingen/endringer under møtene.
 
 

## Deloppgave 2:

### "Stretch goal"
Her ble vi spurt om å bestemme oss for én litt mer avansert ting som skal bli lagt til i spillet. Her har vi bestemt oss for å i alle fall fikse multiplayer på samme maskin. Dette har vi bestemt oss for grunnet at det virker overkommelig for oss å utføre dette innenfor den rimelige tiden vi har for prosjektet. I tillegg har vi diskutert om å porte til en annen platform (Android), noe som vi tenker å prøve å gjøre om vi føler vi har tid nok. 


### MVP og annet
#### Prioriteringsliste (MVPs / krav):
(basert på obligatorisk 1)

1. Vise et spillebrett
2. Vise spiller på spillebrett
3. Flytte spiller (vha taster e.l.)
4. Tyngdekraft på spilleren
5. Spiller interagerer med terreng
6. Start-skjerm ved oppstart
7. Hud som viser poeng
9. Vise fiender/monstre på spillbrettet.
10. Fiender/monstre interagerer med terreng og spiller.
11. Spiller har poeng og interagerer med poenggjenstande
12. Spiller kan dø (ved kontakt med fiender, eller ved å falle utfor skjermen)
13. Game-over skjerm
14. Restart-funksjon 
15. Musikk/sound effects
16. Mål for spillbrett (enten et sted, en mengde poeng, drepe alle fiender e.l.
17. Flere nivåer/brett.
18. Nytt nivå når forrige er ferdig
19. Støtte flere spillere (enten på samme maskin eller over nettverk)
20. Android port (stretch goal)

Dette er foreløpig prioriteringsliste som er basert på de MVP-kravene fra obligatorisk 1, som vi har oppdatert basert på hva vi vil gjøre med produktet. Her har vi lagt til/endret 6, 9, 11, 12, 13, 15, 17.
De kravene vi har gjort så langt er 1-3 (15.03.2022). 

De nye funksjonalitetene vi har lagt til har vi prioritert basert på hvilke tilknytninger funksjonaliteten/kravet har til andre funksjonaliteter/krav. F.eks. punkt 11 har tilknytting til punkt 9 og 10 og vi har da valgt å sette prioriteringen der. Prioriteringen blir altså bestemt gjennom hva som virker mest naturlig å jobbe med videre etter at ett krav er ferdig.


#### Brukerhistorier 
Her skal vi skrive brukerhistorier av de kravene som vi har planer om å få fullført fram til deadline. 

Brukerhistorier for krav 1-3 skrives ikke her, da disse kravene ble fullført i forrige sprint (obligatorisk 1).

- Som spiller trenger jeg en form for tyngdekraft, slik at jeg faller ned på bakken igjen når jeg hopper.
	- Akseptansekriterier:
		- Spiller skal ikke sveve, men heller falle ned i y-retning med en konstant akselerasjon.
		- Når spiller hopper, skal spiller falle ned igjen.
	- Arbeidsoppgaver:
		- Implementere en fast gravity variabel som scaler med fps.
		- Endre y-velocity (hastighet til spiller) med denne gravity-variabelen.

- Som spiller trenger jeg å kunne kollidere med bakken slik at jeg ikke bare faller ned grunnet tyngdekraften.
	- Akseptansekriterier:
		- Spiller kan ikke falle gjennom bakken.
	- Arbeidsoppgaver: 
		- Implementere en klasse som styrer med sjekking av kollisjon
		- Implementere bruk av denne klassen slik at spilleren kolliderer med bakken.

- Som spiller trenger jeg å kunne kollidere med andre objekter på kartet for å interagere med de.
	- Akseptansekriterier:
		- Spiller skal ikke kunne gå gjennom vegger eller lignende.
	- Arbeidsoppgaver:
		- (Lik forrige punkt)

- Som spiller trenger jeg en start-meny for å kunne velge om jeg vil spille eller ikke.
	- Akseptansekriterier:
		- Spillet skal kun starte når spiller trykker på start-knappen (A-GAME) og ikke ellers.
		- Spillet skal lukkes når spiller trykker på "Quit".
	- Arbeidsoppgaver:
		- Implementere en MainMenu-klasse som har metoder for å vise knapper som kjører eller lukker spillet.
		- Fikse en GameScreen-klasse som MainMenu går til for å starte spillet.

- Som spiller trenger jeg fiender for å utfordre meg selv.
	- Akseptansekriterier:
		- Spillet skal vise fiender og de skal ta skade av spiller og motsatt gjennom kollisjon.
		- Fiender beveger seg uavhengig av spiller.
	- Arbeidsoppgaver:
		- Lage en Enemy-klasse som styrer med helse, skade, bevegelse og slikt.
		- Vise fienden på spillbrettet.
	(Vi rakk bare å vise fienden i spillbrettet før deadline, så her står den bare i ro og det er ingen interaksjon med den foreløpig)

- Som spiller trenger jeg en score for å se hvor mange poeng jeg har fått.
	- Akseptansekriterier:
		- Spiller skal kunne se en form av poeng-teller i en av hjørnene i vinduet.
		- Scoren skal oppdateres riktig basert på hva som skjer i spillet.
	- Arbeidsoppgaver:
		- Implementere en Score-klasse har metoder som øker og minker poeng. 
		- Implementere en HUD-klasse som viser poeng i en av hjørnene i spillvinduet.
	


Prioritering av oppgavene er basert på prioriteringslisten skrevet over, hvor mye av fokuset vil bli lagt på å få legge til tyngdekraft og kollisjon med spiller, da dette utgjør mye av spillet. 

Vi har gjort noen få justeringer på MVP-kravene basert på listen vi fikk i obligatorisk 1. Disse endringene har vært å dele opp noen av de eksisterende kravene, i tillegg til at vi har lagt til noen nye og hvor vi også da har endret litt på rekkefølgen av kravene. Grunnen til dette var for å skape en bedre "flyt" mellom kravene og for å ikke gjøre hver av kravene for store.


#### Bugliste
Her er en liste over nåværende bugs som ikke har blitt fikset, men som vi vet om:
- Man kan falle ned for alltid om man går utenfor "skjermen".
- Hvis man flytter seg i en retning og så plutselig flytter seg i annen/motsatt retning, hvor man fortsatt holder tasten for originale retning inn, og så slipper  tasten for den nye retningen, så stopper spilleren opp.
- Kollisjon er litt janky i form av at kollisjonsområdet ikke er i samsvar med sprite størrelse, dette gjør at det ser ut som at spilleren svever.
- Om man tar fullscreen før man trykker start, blir størrelse av textures rare. 


#### Oversikt over hva som har blitt fikset basert på tilbakemelding:
- Lagt til referate av møter.
- Endringer av roller og mer utdypende begrunnelse og forklaring av roller. 
- Endring og mer utdypende forklaring av prosjekt metodikk.
- Fix på Trello:
	- Her har vi slettet mangen unødvendige tavler.
	- Om strukturert hoved tavle
	- Vi har valgt å gå for å ha en "stor oppgave" med mangen små under oppgaver som skal gjøres.
	- Disse underoppgavene kan endres eller flyttes ut til en egen "stor oppgave" om det skal være nødvendig
	- Vi gjorde dette fordet vi fikk inntrykk av at dette var mer oversiktlig