# Oblig 4 – A-Game

* Team: A-Team (Gruppe 4): Oscar Nysten, Audun Haugen, Marius Aasebø, Peder Spooren

## Deloppgave 1:

### Referat av teammøter:

#### 22.04.2022
Tilstede: Marius, Peder

I dette møtet startet vi (de som møtte opp) med splitten, hvor vi først skrev ned dokumentasjonen (denne filen). Som i de forrige splittene, så skrev vi ned prioriteringslisten av de gjenværende MVP-sene, i tillegg til brukerhistorier til disse. Vi la også opp plan for hva vi og hvordan vi skulle gjøre, hvor vi satte to sammen som skal fokusere på de gjenværende MVP-sene, mens de to andre fokuserer på å lage grundige tester.
Vi ble også enige her om å fokusere på å levere ett mest mulig komplett og velfungerende spill som mulig, uten annet ekstra som går utenfor MVP-sene. Vi ble enige om at hvis vi finner tid til noe ekstra, så kan man da prøve seg på å legge til noe utenfor MVP. Videre begynte vi arbeidet med å først fikse bevegelse av kamera avhengig av spillers posisjon, da dette var nødvendig for de neste MVP-sene.

#### 25.04.2022
Tilstede: Audun, Marius, Peder

I dette møtet diskuterte vi videre hva som skulle gjøres, og hvorvidt store og kompliserte testene skal være. Her snakket vi da om at testene skal være så enkle som mulige, og teste alt av spillogikk, i tillegg til alt annet som kan testes. Vi fortsette videre og begynte med ordentlig level progresjon, da vi la til ett tredje level. Vi nevnte også til hverandre noen ideer vi kunne videreutvikle som er utenfor MVP, men vi ble enige, som også nevnt tidligere, at vi prioriterer MVPs, men om vi finner tid til å implementere noe ekstra, så gjør man det om man ønsker. Vi ble også enige på dette møtet om når vi skal holde prosjektpresentasjon.  

#### 26.04.2022
Tilstede: Audun, Oscar, Marius, Peder

I dette møtet fortsatte vi arbeidet vi planla i de tidligere møtene, hvor to av oss fokuserer på tester, mens de to andre fokuserer på å gjøre ferdig de siste kravene for spillet. Vi smålig diskuterte noe ekstra greier vi tenkte kanskje kunne vært lurt å implementere, og ble enige at om vi finner tid mot slutten, så kan vi implementere disse. Vi fokuserte mest på å abstrahere allerede eksisterende klasser som deler kode om til abstrakte klasser. De som jobbet med testene prøvde å finne ut videre hvordan testene skulle utføres, og startet å ramse opp alle testene som skulle legges til. 

#### 27.04.2022
Tilstede: Oscar, Marius, Peder

Dette møtet ble holdt digitalt, hvor vi egentlig jobbet mer for oss selv, men hvor vi diskuterte om folk lurte på noe, eller om folk spurte om input. Her fortsatte vi med abstrahering av klasser, for å klargjøre det som skulle til for å implementere multiplayer, i tillegg til å fikse på ting basert på tilbakemeldingen vi fikk for forrige sprint. Noen problemer som forekom var at vi slet litt med abstraheringen av contact, hvor det ikke ble noe særlig til abstrahering. Vi ble derfor enige om at vi fortsatt beholdte den abstrakte klassen AbstractContact, abstrahere det som var lettest mulig (isCellBlocked), og lar de resterende metodene funke slik som et interface. 

#### 28.04.2022
Tilstede: Audun, Peder, Marius

Her møtte vi bare to stykk opp, hvor en av oss jobbet med tester, og den andre fikset multiplayer. Vi diskuterte om hva som manglet, og om en av oss fikk noe problem, så hørte vi med hverandre om den andres tanker. Vi begynte også å tenke på morgendagen, som er dagen for deadline, hvor vi snakket om hva vi ville fokusere på å gjøre ferdig til slutt. Mot slutten snakket vi om hvilke tester som skulle lages og hvordan disse kunne potensielt gjøres (f.eks. ved å lage test maps for å teste spesifikk funksjonalitet).  

#### 29.04.2022
Tilstede: Oscar, Audun, Peder, Marius

Dette møtet er siste møte før siste deadline, her gjør vi ferdig det vi rekker før vi releaser. Vi diskuterte kjapt hva vi håper på å få gjøre og gir hverandre oppgaver basert på det. Vi fikset også bugs som dukket opp underveis. 

### Oppsummering (for punktene under referat)
Rollene i teamet har holdt seg likt som forrige obligatorisk, altså er rollene fortsatt slik:

Produktansvarlig / Grafikkansvarlig 	- Marius <br>
Physicsansvarlig / kvalitetskontroll	- Oscar <br>
Dokumentasjonsansvarlig / Scrum master  - Peder <br>
Git-expert / Kundekontakt 				- Audun <br>


Disse rollene fungerer fint, og vi velger derfor å beholde strukturen.

#### Rollenes betydning:
Her er rollenes betydning, som er lik de fra forrige obligatorisk.

- Produktansvarlig er personen som har ansvaret til å definere hvordan spillet skal være og se ut. Dette inkluderer også å gjennomgå brukerhistorier, i tillegg til å komme med diverse idéer.

- Grafikkansvarlig er ansvarlig for alt av det grafiske i spillet. Dette gjelder utseende av spillkarakter/spillbrett, menyer og diverse andre "skjermer" (loading screen, game-over screen).

- Physicsansvarlig er ansvarlig for alt om fysikk og movement av spillet og andre objekter i spillet. Dette inkluderer da tyngdekraft, kollisjon og alt av movement til spilleren. 

- Kvalitetskontroll er personen som forsikrer at koden ser ryddig og pen ut, i tillegg til at koden fungerer som den skal i spillet når man kjører det. Her kan man tenke at tester skal gjøre mye av jobben automatisk, men kvalitetskontrolløren skal overse leveransen og de manuelle testene. 

- Dokumentasjonsansvarlig er ansvarlig for alt av dokumentasjon i prosjektet. Denne personen skal ikke skrive dokumentasjonen til metodene/klassene andre personer skriver, men denne personen skal kontrollere om det ser pent, riktig og forståelig ut.

- Scrum master er personen som styrer teamet, alt fra å dirigere møter og holde prosjektet i gang. Denne personen er altså den som tar initiativ, og om det er noe uklart eller uenigheter, så er det scrum master som har veto.

- Git-experten skal ha kontroll over alt som skjer i git og repositoriet. Dette gjelder pushing, pulling, merging og leveransekontroll (sammen med kvalitetskontrollør) for å nevne noe. 

- Kundekontakt er den personen som har kontakt med kunde, enten om det er kunden som har problem, eller om det er vi som etterspør tilbakemelding. 


#### Prosjektmetodikk
Vi har fortsatt Scrum-metodikken som vi utviklet forrige obligatorisk. Dette opplever vi som velfungerende metodikk, som beskrevet i forrige obligatorisk. Vi har brukt Trello aktivt og har satt delmål under hvert "kort". Vi har holdt hverandre oppdatert over hva man har gjort, og korrigert ved behov. 

#### Gruppedynamikk


#### Retrospektiv
Denne sprinten har gått veldig bra. Vi hadde ett godt utgangspunkt med spillet vårt da vi started, men hadde fortsatt noen MVP-s som måtte implementeres. Disse ble implementert rimelig fort, og mye av arbeidet denne sprinten gikk til abstrahering og strukturen av prosjektet, i tillegg til å fikse tester og fikse bugs som var tilstede. Målet var å få ett mest mulig fungerende produkt til slutt, noe som vi føler vi har fått til, spesielt med tanke på hvordan spillet var etter forrige sprinten. 
Vi delte også oss opp i to og to, hvor to av oss jobbet med de siste MVP-kravene, mens de andre to jobbet med tester, da vi manglet en del tester.
Det ble at vi ikke fikk tid til vårt andre stretch goal, som var android port, så da ble det at multiplayer på samme masking ble vårt stretch goal.

Kommunikasjon og gruppedynamikken holdt seg bra også denne sprinten, og har egentlig bare blitt bedre og bedre da vi har bare blitt bedre og bedre kjent med hverandre. push/pull/merging via git gikk fortsatt fint, men som tidliger hadde vi enkelte merge conflicts, som heldigvis ble fikset øyeblikkelig. 

Til nå fikk vi fullføre alle MVP-kravene spillet krevde, noe som er bra. Vi følte vi var fornøyde med spillet slik det ble til denne sprinten.

Med tanke på retrospektiv for hele prosjektet så er det åpenbart at vi kunne gjort ting annerledes. Synes vi satte inn god innsats fra starten av, men vi følte oss rimelig "lost" de to første sprintene. Det var først etter sprint 2 at ting ble klarere, og vi fikk ett mer nøyaktiv mål i prosjektet, som ikke var like abstrakt som tidligere, i tillegg til at vi begynte å bli kjent med libGDX. Vi var også generell flinke med å bruke git, selv om vi hadde noen problemer underveis med conflicts og bare generelle problemer med git. Vi var også flinke på å gi hverandre oppgaver, slik at det ble god fordeling av oppgavene som måtte gjøres. Etter sprint 2 ble vi også veldig flinke å holde møter hvor vi jobbet i fellesskap, og hvor vi hadde diskusjoner. Vi var også flinke med å kommunisere med hverandre om noen lurte på noe eller ville høre hva andre tenkte om en ting. Ingen av oss var redd for å spørre. 

Som nevnt tidligere kunne vi gjort en del ting annerledes. For eksempel, kunne vi ha praktisert bedre testdrevet programmering, da vi jobbet lite med tester før siste sprint. Her skrev vi da egentlig bare kode, og bare kjørte spillet for å se om det ble som vi hadde håpet på, så dette er noe vi kunne gjort bedre. Vi kunne også ha vært litt flinkere til å jobbe mer selvstendig, f.eks. med oppgaver vi ga hverandre, da mesteparten av prosjektet ble gjort i fellesskap, men kan nevne at vi ble flinkere på dette ved siste sprint. Man kan alltid være flinkere mtp. kommunikasjon, men føler vi gjorde en grei jobb der. Noe vi ville ha gjort om vi gjorde prosjektet på nytt, er å komme med en mye klarere plan fra starten av, hvor vi utøvde mer korrekt scrum-metodikk fra starten av (noe som vi ikke gjorde), og hvor vi startet med fokus på tester. 


### Deloppgave 2:

### "Stretch goal"
"Stretch goal"-en er det samme som vi nevnte sist obligatorisk, altså vi bestemte oss for å i alle fall fikse multiplayer på samme masking, i tillegg til at vi tenker å prøve å fikse Android port om vi har tid til det (ble vi ikke rakk det).

### MVP og annet
#### Prioriteringsliste:
1. Mål for spillbrett (enten et sted, en mengde poeng, drepe alle fiender e.l.)
2. Flere nivåer/brett.
3. Nytt nivå når forrige er ferdig.
4. Støtte flere spillere (enten på samme maskin eller over nettverk, stretch goal).

Denne listen er tatt fra obligatorisk 3, hvor de punktene som vi allerede har gjort er fjernet.
Dette er da vår prioriteringsliste for denne splitten, hvor vi skal prøve å få til å implementere så mange som mulig fram til deadline. 

#### Midlertidig plan for punktene
1. Mål for spillbrett (enten et sted, en mengde poeng, drepe alle fiender e.l.)
	- Mål basert på poeng (samle alle rubies/samle x antall poeng).
	- Åpne win-screen når dette forekommer, med mulighet for restart, next level, quit.

2. Flere nivåer/brett.
	- Bare lage flere brett på Tiled

3. Nytt nivå når forrige er ferdig
	- Basert på win-screen. 
	- Ha flere GameScreen-klasser (GameScreenLevel1, GameScreenLevel2 osv.) som brukes til å starte neste level, hvor vi bruker abstract klasse GameScreen som alle GameScreen-klasser arver fra.
	
4. Støtte flere spillere (enten på samme maskin, stretch goal)
	- Lage en abstract klasse Player, som Player1 og Player2-klasser arver fra. I denne abstrakte klassen er alt som er felles mellom players.
	- Finne ut hvordan vi gjør det med kamera og bevegelse mellom spillerne (om ene spilleren går ut av kamera e.l, skal den dø? blokkeres? e.l?)


#### Brukerhistorier:
Her skriver vi brukerhistorier fortløpende som vi skal til å starte med en av punktene over:

- Som spiller trenger jeg mål for spillbrett slik at jeg kan avansere gjennom ulike brett og fullføre spillet.
	- Akseptansekriterier:
		- Spillbrettet skal ha en maks score som spiller kan oppnå som mål.
		- Spillet skal åpne en win-screen ("Level complete" elns) som åpner når spiller oppnår målet.
	- Arbeidsoppgaver:
		- Legge til en WinCondition konstant, som sjekker hele tiden om score er lik denne WinCondition.
		- Lage en LevelCompleteScreen som åpnes når målet er oppnådd. 
		
- Som spiller trenger jeg flere nivået slik at når jeg er ferdig med forrige, så fortsetter jeg til neste.
	- Akseptansekriterier:
		- Nytt spillbrett skal åpnes etter å ha trykket "Next Level" i LevelCompleteScreen.
	- Arbeidsoppgaver:
		- Lage flere brett i Tiled.
		- Legge disse til i levels-mappen og til spillet, slik at nytt nivå åpner etter forrige er ferdig.

- Som spillere trenger vi mulighet til å spille sammen med hver sin karakter slik at vi kan spille sammen.
	- Akseptansekriterier:
		- Skal ha to spillere som beveger seg uavhengig av hverandre.
		- Begge spillere skal ha mulighet til å gjøre det samme.
		- Spillere kan ikke gå ut av kamera (spiller dør eller blir stoppet).
		- Kan bare spille sammen om man trykker på "multiplayer" i hovedmeny.
	- Arbeidsoppgaver:
		- Lage abstrakt klasse Player, som to nye klasser Player1 og Player2 arver fra.
		- Legge til ny knapp "Multiplayer" i hovedmeny.
		- Legge til Player1 og Player2 om "Multiplayer" blir valgt i hovedmeny.
		- Fikse kamera slik at kamera fokuserer på kun Player1.
		- Fikse slik at om Player2 prøver å gå utenfor kamera så blir han stoppet eller dør.
		
Disse MVP-sene er de vi tenker å fullføre før deadline. Dette for å få ett så komplett og velfungerende spill som mulig, uten annen unødvending implementasjoner. Når dette er nevnt, så tenker vi å legge til diverse funksjoner om vi finner tid til dette, men vi prioriterer MVP-sene. 

### Deloppgave 3:
Her vil vi nevne at SpotBugs ble installert og brukt, hvor vi rettet på bugs som den plukket opp der det kunne gjøres. 
		
### Buglist:
- Hvis man flytter seg i en retning og så plutselig flytter seg i annen/motsatt retning, hvor man fortsatt holder tasten for originale retning inn, og så slipper  tasten for den nye retningen, så stopper spilleren opp.
- HUD-en (poeng/helse teller) skaler dårlig med størrelsen på vinduet.

Fixed bugs:
- Fikset scaling av spill og buttons for fullscreen/endring av størrelse på spillvinduet.
- Sprites og textures mtp. kollisjon er nå bedre.
- Fikset bugs hvor screens overlappet og ble stygt, fikset på dispose.
- Fikset at previous knappen i WinScreen fungerte som den skal.

#### Oversikt over hva som har blitt fikset basert på tilbakemelding:
- 

