package overlay;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;

import Values.Health;
import Values.Score;
import entities.AbstractPlayer;
import entities.Player1;
import entities.Player2;


/**
 * Hud class which governs the head-up display, shows and updates score gained by player.
 */
public class Hud implements Disposable {
	public Stage stage;
	
	public static Score score;
	private static Label scoreLabel;
	private static Label pointLabel;
	
	public static Health health;
	private static Label healthLabel;
	private static Label hpLabel;
	
	private LabelStyle labelStyle;
	
	public Hud(SpriteBatch sb) {
		score = new Score(0);
		stage = new Stage();
		
		health = new Health(Player1.health);
		
		Table tableScore = new Table();
		Table tableHealth = new Table();
		tableScore.top();
		tableScore.setFillParent(true);
		tableHealth.top();
		tableHealth.setFillParent(true);
		
		labelStyle = new Label.LabelStyle(new BitmapFont(), Color.WHITE);
		
		pointLabel = new Label(String.format("%03d", score.getValue()), labelStyle);
		scoreLabel = new Label("Score:", labelStyle);
		
		healthLabel = new Label("Health: ", labelStyle);
		hpLabel = new Label(String.format("%01d", health.getValue()), labelStyle);
		
		tableScore.add(scoreLabel).expandX().padLeft(565);
		tableScore.add(pointLabel);
		
		tableHealth.add(healthLabel).expandX();
		tableHealth.add(hpLabel).padRight(580);
		
		stage.addActor(tableScore);
		stage.addActor(tableHealth);
	}
	
	/**
	 * Increments and updates score for HUD.
	 * @param value
	 */
	public static void scoreIncrementHUD(int value) {
		score.valueIncrement(value);
		pointLabel.setText(String.format("%03d", score.getValue()));
	}
	
	/**
	 * Decrements and updates score for HUD.
	 * @param value
	 */
	public static void scoreDecrementHUD(int value) {
		score.valueDecrement(value);
		pointLabel.setText(String.format("%03d", score.getValue()));
	}
	
	/**
	 * Decrements and updates player health for HUD.
	 */
	public static void healthDecreaseHUD() {
		health.valueDecrement(1);
		hpLabel.setText(String.format("%01d", health.getValue()));
	}
	
	@Override
	public void dispose() {
		stage.dispose();
		
		
	}

}
