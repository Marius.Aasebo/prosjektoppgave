package Values;

public abstract class AbstractValue {
	public int value;
	
	/**
	 * Increase value by given amount.
	 * @param value
	 */
	void valueIncrement(int value) {
		this.value += value;
	}
	
	/**
	 * Decrease value by given amount.
	 * @param value
	 */
	public void valueDecrement(int value) {
		if(this.value <= value)
			this.value = 0;
		else
			this.value -= value;
	} 
	
	/**
	 * Sets the value to an arbitrary value.
	 * @param value
	 * @return int - the new value
	 */
	int setValue(int value) {
		return this.value = value;
	}
	
	/**
	 * Get the current value
	 * @return int - the current value
	 */
	public int getValue() {
		return value;
	}

}
