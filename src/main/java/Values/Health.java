package Values;

/**
 * Health class implementing interface IValue.
 * Governs the health to entity, includes methods for increasing, decreasing, setting and getting health to entity.
 */
public class Health extends AbstractValue {
	
	public Health(int health) {
		value = health;
	}

}
