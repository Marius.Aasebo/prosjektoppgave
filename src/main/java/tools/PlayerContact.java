package tools;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import Values.Score;
import entities.AbstractPlayer;
import entities.Enemy;
import overlay.Hud;

/**
 * PlayerContact class implements interface IContact.
 * Contains methods governing contact/collision between player and other objects/entities in game.
 */
public class PlayerContact extends AbstractContact {
	private AbstractPlayer player;
	
	// Unique ruby key for rubies, used for player-ruby interaction where player can "pick up" rubies and gain points.
    String ruby = "ruby";
    
    public PlayerContact(AbstractPlayer player){
        this.player = player;
    }
	
	/**
     * Checks the enemy-player contact where enemy and player come into contact from the sides, 
     * in other words, collision where player doesn't jump on enemy
     * @param enemy
     * @param player
     * 
     * @return boolean - true if player comes into contact with enemy by not jumping on enemy.
     */
	public static boolean enemyOnPlayerContact(Enemy enemy, AbstractPlayer player) {
		
		int cellPlayerX = ((int) ((player.getX()) / player.collisionLayer.getTileWidth()));
		int cellPlayerY = ((int) ((player.getY()) / player.collisionLayer.getTileHeight()));
		
		int cellEnemyX = ((int) ((enemy.getX()) / enemy.collisionLayer.getTileWidth()));
		int cellEnemyY = ((int) ((enemy.getY()) / enemy.collisionLayer.getTileHeight()));
		
		return enemyKillPlayer(cellPlayerX, cellPlayerY, cellEnemyX, cellEnemyY);
		
	}
	
	/**
	 * Checks the enemy-player contact where enemy and player comes into contact from top,
	 * in other words, collision where player jumps on enemy.
	 * @param enemy
	 * @param player
	 * @return boolean - true if player comes into contact with enemy by jumping on enemy.
	 */
	public static boolean playerOnEnemyContact(Enemy enemy, AbstractPlayer player) {
		
		int cellPlayerX = ((int) ((player.getX()) / player.collisionLayer.getTileWidth()));
		int cellPlayerY = ((int) ((player.getY()) / player.collisionLayer.getTileHeight()));
		
		int cellEnemyX = ((int) ((enemy.getX()) / enemy.collisionLayer.getTileWidth()));
		int cellEnemyY = ((int) ((enemy.getY()) / enemy.collisionLayer.getTileHeight()));
		
		return playerKillEnemy(cellPlayerX, cellPlayerY, cellEnemyX, cellEnemyY);
	}

	/**
	 * Compares player and enemy coordinates and checks if player has same x-coordinates but is one cell above enemy.
	 * @param cellPlayerX
	 * @param cellPlayerY
	 * @param cellEnemyX
	 * @param cellEnemyY
	 * @return true - if player is right above enemy.
	 */
	private static boolean playerKillEnemy(int cellPlayerX, int cellPlayerY, int cellEnemyX, int cellEnemyY) {
		if((cellPlayerX == cellEnemyX) && (cellPlayerY == (cellEnemyY + 1))) {
			return true;
		}
		return false;
	}

	/**
	 * Compares player and enemy coordinates and checks if player has same x/y-coordinates as the enemy.
	 * @param cellPlayerX
	 * @param cellPlayerY
	 * @param cellEnemyX
	 * @param cellEnemyY
	 * @return true - if player and enemy has the same x/y-coordinates
	 */
	private static boolean enemyKillPlayer(int cellPlayerX, int cellPlayerY, int cellEnemyX, int cellEnemyY) {
		if((cellPlayerX == cellEnemyX) && (cellPlayerY == cellEnemyY)) {
			return true;
		}
		return false;
	}
	
	/**
     * Checks if a specific cell has keyword "ruby"
     * Used for point system
     * @param x - x position
     * @param y - y position
     * @return True if cell has keyword "ruby"
     */
    private boolean isCellRuby(float x, float y) {
        TiledMapTileLayer.Cell cell = player.collisionLayer.getCell((int) (x / player.collisionLayer.getTileWidth()), (int) (y / player.collisionLayer.getTileHeight()));
        return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(ruby);
    }

	@Override
	public boolean collidesRight() {
		for(float step = 0; step <= player.getHeight(); step += player.increment)
            if(isCellBlocked(player.getX() + player.getWidth(), player.getY() + step, player))
                return true;
        	
            else if(isCellRuby(player.getX() + player.getWidth(), player.getY() + step)) {
            	TiledMapTileLayer.Cell cell = player.collisionLayer.getCell((int) ((player.getX() + player.getWidth()) / player.collisionLayer.getTileWidth()), (int) ((player.getY() + step) / player.collisionLayer.getTileHeight()));
            	cell.setTile(null);
            	Hud.scoreIncrementHUD(Score.rubyValue);
            	return false;
            }
        
        return false;
	}

	@Override
	public boolean collidesLeft() {
        for(float step = 0; step <= player.getHeight(); step += player.increment)
            if(isCellBlocked(player.getX(), player.getY() + step, player))
                return true;
            
            else if(isCellRuby(player.getX(), player.getY() + step)) {
            	TiledMapTileLayer.Cell cell = player.collisionLayer.getCell((int) (player.getX() / player.collisionLayer.getTileWidth()), (int) ((player.getY() + step) / player.collisionLayer.getTileHeight()));
            	cell.setTile(null);
            	Hud.scoreIncrementHUD(Score.rubyValue);
            	return false;
            }
        
        return false;
    }

	@Override
	public boolean collidesTop() {
        for(float step = 0; step <= player.getWidth(); step += player.increment)
            if(isCellBlocked(player.getX() + step, player.getY() + player.getHeight(), player))
                return true;
            
            else if(isCellRuby(player.getX() + step, player.getY() + player.getHeight())) {
            	TiledMapTileLayer.Cell cell = player.collisionLayer.getCell((int) ((player.getX() + step) / player.collisionLayer.getTileWidth()), (int) ((player.getY() + player.getHeight()) / player.collisionLayer.getTileHeight()));
            	cell.setTile(null);
            	Hud.scoreIncrementHUD(Score.rubyValue);
            	return false;
            }
        
        return false;

    }

	@Override
	public boolean collidesBottom() {
        for(float step = 0; step <= player.getWidth(); step += player.increment)
            if(isCellBlocked(player.getX() + step, player.getY(), player))
                return true;
            
            else if(isCellRuby(player.getX() + step, player.getY())) {
                TiledMapTileLayer.Cell cell = player.collisionLayer.getCell((int) ((player.getX() + step) / player.collisionLayer.getTileWidth()), (int) (player.getY() / player.collisionLayer.getTileHeight()));
                cell.setTile(null);
            	Hud.scoreIncrementHUD(Score.rubyValue);
                return false;
            }

        
        return false;
    }

}
