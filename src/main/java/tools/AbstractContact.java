package tools;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import entities.AbstractEntity;

public abstract class AbstractContact {
	String blockedKey = "blocked";
	
	public boolean isCellBlocked(float x, float y, AbstractEntity entity) {
		TiledMapTileLayer.Cell cell = entity.collisionLayer.getCell((int) (x / entity.collisionLayer.getTileWidth()), (int) (y / entity.collisionLayer.getTileHeight()));
		return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(blockedKey);
	}
	
	abstract boolean collidesRight();
	
	abstract boolean collidesLeft();
	
	abstract boolean collidesTop();
	
	abstract boolean collidesBottom();

}
