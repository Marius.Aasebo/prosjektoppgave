package entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import tools.EnemyContact;


/**
 * Enemy class, similar to the Player class, includes collision and movement of enemies.
 */
public class Enemy extends AbstractEntity {
	
	private float direction;
	private final EnemyContact cont = new EnemyContact(this);

	public Enemy(Sprite sprite, TiledMapTileLayer collisionLayer) {
		super(sprite);
		name = "B-Spider";
		speed = 100;
		direction = speed;
    	this.collisionLayer = collisionLayer;
	}
	

	@Override
	public boolean checkCollisionX(boolean collisionX) {
		if(velocity.x < 0) // going left
			collisionX = cont.collidesLeft();
		else if(velocity.x > 0) // going right
			collisionX = cont.collidesRight();
		return collisionX;	
	}

	/**
	 * Additional functionality for enemy:
	 * If it collides, it gets set on a different direction based on which way it collides. 
	 */
	@Override
	public void reactCollisionX(float oldX, boolean collisionX) {
		if(collisionX) {
            if(collisionX = cont.collidesLeft()) {
            	setX(oldX);
            	direction = speed;
            	velocity.x = direction;
            }
            else if(collisionX = cont.collidesRight()) {
            	setX(oldX);
            	direction = -speed;
            	velocity.x = direction;
            }
        }
        else {
        	velocity.x = direction;
        }
	}

	@Override
	public boolean checkCollisionY(boolean collisionY) {
		if(velocity.y < 0) // going down
            collisionY = cont.collidesBottom();
		return collisionY;
	}

}
