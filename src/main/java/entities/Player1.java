package entities;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

import Values.Health;
import tools.PlayerContact;

public class Player1 extends AbstractPlayer implements InputProcessor {

	public Player1(Sprite sprite, TiledMapTileLayer collisionLayer) {
		super(sprite, collisionLayer);
		name = "A-Player";
		health = 3;
		playerHealth = new Health(health);
		startPos = new Vector2(64, 100);
		this.collisionLayer = collisionLayer;
		cont = new PlayerContact(this);
		this.left = new Texture("assets/data/textures/player1Left.png");
    	this.right = new Texture("assets/data/textures/player1Right.png");
    	this.jump = new Texture("assets/data/textures/player1Jump.png");
	}

	@Override
	public boolean keyDown(int keycode) {
		switch(keycode) {
		case Keys.SPACE:
		case Keys.W:
				
			if(canJump) {
				setTexture(jump);
				velocity.y = speed / 1.8f;
				canJump = false;
				jumpSound.play(0.05f);
			}
			break;
		
		case Keys.A:
			setTexture(left);
			velocity.x = -speed;
			break;
				
		case Keys.D:
			setTexture(right);
			velocity.x = speed;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
		case Keys.W:
		case Keys.SPACE:
			setTexture(right);
			break;
		case Keys.A:
		case Keys.D:
			velocity.x = 0;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(float amountX, float amountY) {
		// TODO Auto-generated method stub
		return false;
	}

}
