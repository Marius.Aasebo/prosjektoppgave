package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.viewport.Viewport;
import app.AGame;
import screens.levels.level1;
import screens.levels.level2;
import screens.levels.level3;

/**
 * Abstract class AbstractMenu which has all code shared between menu-classes where menus are built and rendered,
 * while code that is unique to each menu-class is held in each respective class.
 */
public abstract class AbstractMenu implements Screen{
	AGame game;
	final int stillPicY = 350;
	
	final int buttonX = 250;
	final int button1Y = 250;
	final int button2Y = 200;
	final int button3Y = 140;
	final int button4Y = 50;
	
	public Texture stillPic;
	
	public Texture singlePlayer;
	public Texture multiPlayer;
	public Texture singleInactive;
	public Texture multiInactive;
	
	public Texture button1Active;
	public Texture button1Inactive;
	public Texture button2Active;
	public Texture button2Inactive;
	public Texture backGroundTexture;
	
	//A counter used for counting which level the game is on. 1 = level 1, 2 = level 2, 3 = level 3.
	public static int counter = 1;
	public Viewport viewport;
	
	
	public AbstractMenu(AGame game) {
		this.game = game;
	}
	
	/**
	* Method that lights up button when cursor hovers over.
	* @param image 
	* @param posY
	* @return True if cursor is over button
	*/
	public Boolean buttonHover(Texture image, int posY) {
		float inputX = AGame.cam.getInputInGameWorld().x;
		float inputY = AGame.cam.getInputInGameWorld().y;
		int height = image.getHeight();
		int width = image.getWidth();
		int gameHeight = AGame.Height;
		float centeredX = centerX(image);
		
		boolean placement = (inputX < centeredX + width && inputX > centeredX && gameHeight - inputY < posY + height && gameHeight - inputY > posY);
		
		return placement; 
	}
	
	
	public void render(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
    	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    	game.batch.begin();
        game.batch.draw(stillPic, centerX(stillPic), stillPicY);
        game.batch.end();
		
	}
	/**
	* Checks if quit button is clicked.
	* @param image
	* @param image  
	* @param posY
	* @param posX
	* @param Agame game
	* 
	*/
	public void touchCheckQuit(Texture active, Texture inactive, int posY, int posX, AGame game) {
		float centeredX = centerX(active);
		if(buttonHover(active, posY)) {
        	game.batch.draw(active, centeredX, posY);
      
        	if(Gdx.input.isTouched()) {
        		Gdx.app.exit();
        	} 
        	
        }else {
        	game.batch.draw(inactive, centeredX, posY);
        }
	}
	
	/**
	* Checks if quit button is clicked.
	* @param image
	* @param image  
	* @param posY
	* @param posX
	* @param Agame game
	* 
	*/
	public void touchCheckMainMenu(Texture active, Texture inactive, int posY, int posX, AGame game) {
		float centeredX = centerX(active);
		if(buttonHover(active, posY)) {
        	game.batch.draw(active, centeredX, posY);
      
        	if(Gdx.input.isTouched()) {
        		this.dispose();
        		game.setScreen(new MainMenu(game));
        	} 
        	
        }else {
        	game.batch.draw(inactive, centeredX, posY);
        }
	}	
	
	/**
	* Checks if level change button is clicked.
	* @param image
	* @param image  
	* @param posY
	* @param posX
	* @param String (level)
	* @param Agame game
	* 
	*/
	public boolean touchCheckLevelChange(Texture active, Texture inactive, int posY, int posX, AGame game) {
		float centeredX = centerX(active);
		if(buttonHover(active, posY)) {
        	game.batch.draw(active, centeredX, posY);
        	
        	if(Gdx.input.isTouched()) {
        		//Checks if button is continue or previous in WinScreen and increases level counter based on that.
        		if(checkButton(active, "assets/data/textures/continue.png")) {
        			counter++;
        			levelCheck(game);
        			return true;
        		}
        		else if(checkButton(active, "assets/data/textures/previous.png")) {
        			//If you press previous in level 1, you restart level 1.
        			if(counter > 1) {
        				counter--;
        			}
        			levelCheck(game);
            		return true;
        		}
        		else {
	        		levelCheck(game);
	        		return true;
        		}
        	} 
        }else {
        	game.batch.draw(inactive, centeredX, posY);
        }
		return false;
	}
	
	/**
	 * Checks which level the player is supposed to be in based on the level counter,
	 * as well as starting up that level using game.setScreen().
	 * @param game
	 */
	private void levelCheck(AGame game) {
		if(counter == 1) {
			this.dispose();
			game.setScreen(new level1(game));
		}
		if(counter == 2) {
			this.dispose();
    		game.setScreen(new level2(game));
		}
		if(counter == 3) {
			this.dispose();
    		game.setScreen(new level3(game));
		}
	}

	protected float centerX(Texture image) {
		return (AGame.Width - image.getWidth()) / 2;
	}
	
	private boolean checkButton(Texture button, String path) {
		return button.toString().equals(path);
	}


}

	
	

