package screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import Values.Score;
import app.AGame;
import entities.Enemy;
import entities.Player1;
import entities.Player2;
import overlay.Hud;
import tools.PlayerContact;

/**
 * Abstract class which has everything that builds and renders a level.
 * This abstract class contains all code that is shared between level-classes where levels are built and rendered,
 * while code that is unique to each level-class is held in each respective class.
 */
public abstract class AbstractGameScreen implements Screen {
	//multiplayerBit for checking if game is run in multiplayer or not. 0 = singleplayer, 1 = multiplayer.
	public static int multiplayerBit;
	
	public AGame game;
    protected TiledMap map;
    protected OrthogonalTiledMapRenderer renderer;
    public OrthographicCamera camera;
    
    public Player1 player1;
    public Player2 player2;
    protected ArrayList<Enemy> enemies;
    
    protected Hud hud;
 
    public Score score;

    Music gameMusic;
    
    public Sprite backGround;
	public SpriteBatch spriteBatch;
	public Texture backGroundTexture;
	
	public AbstractGameScreen() {
		
		camera = new OrthographicCamera();
		
		enemies = new ArrayList<Enemy>();
	}
	
	
    @Override
    public void show() {
    	
    	//InputMultiplexer in case multiplayer is chosen, works for both singleplayer and multiplayer.
	    InputMultiplexer multiplexer = new InputMultiplexer();
	    multiplexer.addProcessor(player1);
	    multiplexer.addProcessor(player2);
	    Gdx.input.setInputProcessor(multiplexer);
	    
	    //Sets position to both players in case of multiplayer, works even if player2 isn't initiated.
		player1.setPosition(player1.startPos.x, player1.startPos.y);
		player2.setPosition(player2.startPos.x, player2.startPos.y);
    	
        gameMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/data/music/gameMusic.mp3"));
        gameMusic.setLooping(true);
        gameMusic.setVolume(0.15f);
        gameMusic.play();
    }

    @Override
    public void render(float delta) {
    	Gdx.gl.glClearColor(135/255f, 206/255f, 235/255f, 1);
    	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    	
        cameraPosition();
        camera.update();
        
        renderer.setView(camera);
        renderer.render();
     
        hud.stage.draw();

        renderer.getBatch().begin();
        
        
        if(multiplayerBit == 0) 
        	player1.drawItem((SpriteBatch) renderer.getBatch());
        else if(multiplayerBit == 1){
        	player1.drawItem((SpriteBatch) renderer.getBatch());
        	player2.drawItem((SpriteBatch) renderer.getBatch());
        }
        renderEnemies();
        removeEntityBasedOnContact();
        
        renderer.getBatch().end();
		
        checkPlayerDead();
        
        goToMainMenu(); 
        
        checkWinCon();

    }

	/**
	 * Fixes camera position to follow player.
	 * If in multiplayer, the camera will follow the player furthest to the right (with the largest x-coordinate value).
	 */
	private void cameraPosition() {
		if(player2.getX() > player1.getX() && player2.getX() > 256)
        	camera.position.set(player2.getX() + player2.getWidth() / 2, (player2.getY() + player2.getHeight() / 2) + 64, 0);
        else if(player1.getX() > 256)
        	camera.position.set(player1.getX() + player1.getWidth() / 2, (player1.getY() + player1.getHeight() / 2) + 64, 0);
        else
        	camera.setToOrtho(false, camera.viewportWidth, camera.viewportHeight);
	}

	/**
	 * Checks if win condition is met, and if so, initiates the WinScreen.
	 */
	private void checkWinCon() {
		if(score.getValue() == 10) {
        	this.dispose();
        	game.setScreen(new WinScreen(game));
        }
	}

	
	/**
	 * Takes the player to the main menu if player presses ESCAPE/ESC on the keyboard.
	 */
	private void goToMainMenu() {
		if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
    		this.dispose();
    		game.setScreen(new MainMenu(game));
    	}
	}

	/**
	 * Checks if player is dead based on method isPlayerDead() in Player-class.
	 * If player is dead, the game takes the player to the game over screen.
	 */
	private void checkPlayerDead() {
		if(player1.isPlayerDead() || player2.isPlayerDead()){
            this.dispose();
            game.setScreen(new GameOverScreen(game));
        }
	}

	/**
	 * Renders enemies into the game/level.
	 */
	private void renderEnemies() {
		for(Enemy enemy : enemies) {
        	enemy.drawItem( (SpriteBatch) renderer.getBatch());
        }
	}

	/**
	 * Removes entity (player or enemy) based on how they contact with each other.
	 * Enemies die if player "jump on" them.
	 * Player loses health on any other form of contact with enemy.
	 */
	private void removeEntityBasedOnContact() {
		for(int i = 0; i < enemies.size(); i++) {
			if(PlayerContact.playerOnEnemyContact(enemies.get(i), player1) || PlayerContact.playerOnEnemyContact(enemies.get(i), player2)) {
				Hud.scoreIncrementHUD(5);
				enemies.remove(i);
			}
			else if(PlayerContact.enemyOnPlayerContact(enemies.get(i), player1)) 
				player1.playerTakesDamage();
			else if(PlayerContact.enemyOnPlayerContact(enemies.get(i), player2)) 
				player2.playerTakesDamage();
        }
	}

    @Override
    public void resize(int width, int height) {
    	camera.viewportWidth = width;
    	camera.viewportHeight = height;
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    	map.dispose();
        renderer.dispose();
        player1.getTexture().dispose();
        player2.getTexture().dispose();
        for(Enemy enemy : enemies)
        	enemy.getTexture().dispose();
        gameMusic.dispose();
    }

}

