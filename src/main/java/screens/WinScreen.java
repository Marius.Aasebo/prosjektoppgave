package screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import app.AGame;

/**
 * WinScreen extending AbstractMenu.
 * This class holds everything unique to the WinScreen, as well as rendering it.
 */
public class WinScreen extends AbstractMenu {
	
	private Music winScreenMusic;
	
	public WinScreen (AGame game) {
		super(game);
		
		stillPic = new Texture("assets/data/textures/levelComplete.png");
		button1Active = new Texture("assets/data/textures/continue.png");
		button1Inactive = new Texture("assets/data/textures/continueInactive.png");
		button2Active = new Texture("assets/data/textures/previous.png");
		button2Inactive = new Texture("assets/data/textures/previousInactive.png");
		
	}

	@Override
	public void show() {
		winScreenMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/data/music/winScreenMusic.mp3"));
		winScreenMusic.setLooping(true);
		winScreenMusic.setVolume(0.5f);
		winScreenMusic.play();
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);

		game.batch.begin();

		touchCheckLevelChange(button1Active, button1Inactive, button2Y, buttonX, game);
        
        touchCheckLevelChange(button2Active, button2Inactive, button4Y, buttonX, game);
        
        game.batch.end();
		
	}

	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		winScreenMusic.dispose();
	}

}