package screens.levels;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import app.AGame;
import entities.Enemy;
import entities.Player1;
import entities.Player2;
import overlay.Hud;
import screens.AbstractGameScreen;

/**
 * GameScreen-class for level3, extends AbstractGameScreen.
 * Holds all code unique or necessary for level3.
 */
public class level3 extends AbstractGameScreen {
	
	public level3(AGame game) {
        this.game = game;
        
        map = new TmxMapLoader().load("assets/data/levels/Level3.tmx");
    	renderer = new OrthogonalTiledMapRenderer(map);
    	
    	//Makes both player objects in case of multiplayer, works even if player2 isn't used further (in singleplayer).
    	player1 = new Player1(new Sprite(new Texture("assets/data/textures/player1Right.png")) , (TiledMapTileLayer) map.getLayers().get(0));
    	player2 = new Player2(new Sprite(new Texture("assets/data/textures/player2Right.png")) , (TiledMapTileLayer) map.getLayers().get(0));
    	
        for(int i = 0; i < 12; i++) {
        	enemies.add(new Enemy(new Sprite(new Texture("assets/data/textures/spider.png")), (TiledMapTileLayer) map.getLayers().get(0)));
        }
    	
        hud = new Hud(game.batch);
        score = Hud.score;
        
    }
	
	@Override
	public void show() {
		super.show();
        enemies.get(0).setPosition(640, 128);
        enemies.get(1).setPosition(1216, 448);
        enemies.get(2).setPosition(1280, 64);
        enemies.get(3).setPosition(1728, 64);
        enemies.get(4).setPosition(3008, 512);
        enemies.get(5).setPosition(4032, 320);
        enemies.get(6).setPosition(4416, 128);
        enemies.get(7).setPosition(5248, 128);
        enemies.get(8).setPosition(4928, 512);
        enemies.get(9).setPosition(7040, 704);
        enemies.get(10).setPosition(7104, 320);
        enemies.get(11).setPosition(8512, 192);
        
	}


}
