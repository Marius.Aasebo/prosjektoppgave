package screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import app.AGame;

/**
 * GameOverScreen class which shows a game over screen when player dies.
 * Gives option for restarting game or quit game.
 */
public class GameOverScreen extends AbstractMenu {
	
	public Texture gameOver;

	private Music gameOverMusic;

	public GameOverScreen (AGame game) {
		super(game);
		
		stillPic = new Texture("assets/data/textures/gameOverStill.png");
		button1Active = new Texture("assets/data/textures/again.png");
		button1Inactive = new Texture("assets/data/textures/againInactive.png");
		button2Active = new Texture("assets/data/textures/quit.png");
		button2Inactive = new Texture("assets/data/textures/quitInactive.png");
		

	}

	@Override
	public void show() {
		gameOverMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/data/music/menuMusic.mp3"));
		gameOverMusic.setLooping(true);
		gameOverMusic.setVolume(0.5f);
		gameOverMusic.play();
	}
	
	
	@Override
	public void render(float delta) {
		super.render(delta);
        game.batch.begin();
        
        touchCheckLevelChange(button1Active, button1Inactive, button2Y, buttonX, game);
        
        touchCheckQuit(button2Active, button2Inactive, button4Y, buttonX, game);
        
        game.batch.end();
		
	}

	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		gameOverMusic.dispose();
	}

}