package screens;


import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import app.AGame;

/**
 * Main menu class which is the main menu that shows when executing Main.java, or when player presses ESCAPE/ESC.
 */
public class MainMenu extends AbstractMenu {
	
	private final Music music = Gdx.audio.newMusic(Gdx.files.internal("assets/data/music/menuMusic.mp3"));


	public MainMenu (AGame game) {
		super(game);
		
		stillPic = new Texture("assets/data/textures/aGame.png");
		
		multiPlayer = new Texture("assets/data/textures/multi.png");
		singlePlayer = new Texture("assets/data/textures/single.png");
		singleInactive = new Texture("assets/data/textures/singleInactive.png");
		multiInactive = new Texture("assets/data/textures/multiInactive.png");
		
		button1Active = new Texture("assets/data/textures/start.png");
		button1Inactive = new Texture("assets/data/textures/startInactive.png");
		button2Active = new Texture("assets/data/textures/quit.png");
		button2Inactive = new Texture("assets/data/textures/quitInactive.png");
		
	}

	@Override
	public void show() {
		counter = 1;
		music.setLooping(true);
		music.setVolume(0.5f);
		music.play();
	}
	
	
	@Override
	public void render(float delta) {
        super.render(delta);
		
        game.batch.begin();
        
        touchCheckQuit(button2Active, button2Inactive, button4Y, buttonX, game);
        
        if(touchCheckLevelChange(singlePlayer, singleInactive, button1Y, buttonX, game))
        	AbstractGameScreen.multiplayerBit = 0;
        
        if(touchCheckLevelChange(multiPlayer, multiInactive, button3Y, buttonX, game))
        	AbstractGameScreen.multiplayerBit = 1;
        
        game.batch.end();
        
		
	}
	
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		music.dispose();
	}

}
