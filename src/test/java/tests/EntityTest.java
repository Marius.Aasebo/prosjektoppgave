package tests;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import entities.Enemy;
import entities.Player1;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tests.testenviroment.TestGame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EntityTest {

    static TestGame game = new TestGame();
    private Player1 p1;
    private Enemy enemy;

    /**
     * Static method run before everything else
     */
    @BeforeAll
    static void setUpBeforeAll() {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setWindowedMode(TestGame.V_Width, TestGame.V_Height);
        new Lwjgl3Application(game, cfg);

    }

    @BeforeEach
    void setUp(){
        TiledMap map = new TmxMapLoader().load("assets/data/levels/Level1.tmx");
        p1 = new Player1(new Sprite(new Texture("assets/data/textures/player1Right.png")), (TiledMapTileLayer) map.getLayers().get(0));
        enemy = new Enemy(new Sprite(new Texture("assets/data/textures/spider.png")), (TiledMapTileLayer) map.getLayers().get(0));
    }

    @Test
    void testPlayerOneName(){
        assertEquals("A-Player", p1.getName());
    }

    @Test
    void testSpiderName(){
        assertEquals("B-Spider", enemy.getName());
    }

    @Test
    void testPlayerMovesRight(){
        p1.setPosition(64,100);
        float startPos = p1.getX();
        p1.velocity.x = 0.5F;
        p1.moveX(10.5F);
        float newPos = p1.getX();
        assertTrue(startPos < newPos);
    }

    @Test
    void testPlayerMovesLeft(){
        p1.setPosition(64,100);
        float startPos = p1.getX();
        p1.velocity.x = -0.5F;
        p1.moveX(1F);
        float newPos = p1.getX();
        assertTrue(startPos > newPos);
    }

    @Test
    void testPlayerMovesToStartOnDamage(){
        p1.setPosition(100,100);
        p1.playerTakesDamage();
        float newX = p1.getX();
        float newY = p1.getY();
        assertEquals(64,newX);
        assertEquals(100,newY);
    }

    @Test
    void testGravityOnPlayer(){
//        p1.setPosition(100,200);
//        p1.velocity.y = 0.5F;
//        p1.gravity= 100F;
//        p1.applyGravity(2F);
//        System.out.println(p1.getY());
//        assertTrue(p1.getY() < 200);
    }

    @Test
    void testCollision(){
//        p1.setPosition(100,100);
//        enemy.setPosition(120,100);
//        p1.velocity.x = 0.5F;
//        p1.moveX(20F);
////        p1.checkCollisionX(true);
//        assertTrue(p1.isDead);
    }
}
