package tests;


import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tests.testenviroment.TestGame;


public class ScreenTest {

    static TestGame game = new TestGame();
    TiledMap map;

    /**
     * Static method run before everything else
     */
    @BeforeAll
    static void setUpBeforeAll() {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setWindowedMode(TestGame.V_Width, TestGame.V_Height);
        new Lwjgl3Application(game, cfg);

    }

    @BeforeEach
    void setUp(){
        map = new TmxMapLoader().load("assets/data/levels/Level1.tmx");
    }


    @Test
    void testStartScreen(){
    }

}
