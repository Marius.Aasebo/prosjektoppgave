package tests;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import entities.Player1;
import Values.Score;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tests.testenviroment.TestGame;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ScoreHealthTest {

    static TestGame game = new TestGame();
    private Player1 p1;
    private Score score;
    TiledMap map;

    /**
     * Static method run before everything else
     */
    @BeforeAll
    static void setUpBeforeAll() {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setWindowedMode(TestGame.V_Width, TestGame.V_Height);
        new Lwjgl3Application(game, cfg);

    }

    @BeforeEach
    void setUp(){
        map = new TmxMapLoader().load("assets/data/levels/Level1.tmx");
        p1 = new Player1(new Sprite(new Texture("assets/data/textures/player1Right.png")), (TiledMapTileLayer) map.getLayers().get(0));
        score = new Score(5);
    }

    @Test
    void testScoreIncrement(){
        int startScore = score.getValue();
        score.valueIncrement(1);
        int newScore = score.getValue();
        assertTrue(newScore > startScore);
    }

    @Test
    void testScoreDecrement(){
        int startScore = score.getValue();
        score.valueDecrement(1);
        int newScore = score.getValue();
        assertTrue(newScore < startScore);
    }

    @Test
    void testPlayerTakesDamage(){
        int startHP = p1.getPlayerHealth();
        p1.playerTakesDamage();
        int newHP = p1.getPlayerHealth();
        assertTrue(startHP > newHP);
    }

    @Test
    void testPlayerContactRuby(){
    }
}
