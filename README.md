# INF112 Group Project 
## Gruppe 4 - Team 1

### Team
Product manager / Graphics manager 	        - Marius Aasebø <br>
Game mechanics manager / Quality controller	- Oscar Nysten<br>
Documentation manager / Scrum master        - Peder Spooren<br>
Git-expert / Customer contact 				- Audun Haugen<br>


### Description
This project is a schoolproject where we make a super mario-like game. In our case, we're making the game, A-Game. A simple platform game.

The goal with this project is to learn how to work in teams, using project methodology which is what is being done in modern software development. This is supposed to give us a insight on work environments in software development. 

Current functions:
- Simple physics system
- Movement of player

The project is set up with Maven, libGdx and Tiles is being used as map editor.

### Requirements
Java 8+ <br>
Java IDE (Eclipse, IntelliJ or something similar) <br>
Version Control, git etc. <br>
Maven <br>

### Windows
Install a Java IDE, such as Eclipse or IntelliJ.

Import the project from version control (git), as a Maven project. Specify pom.xml as the configuration file for Maven.

Run the project, the main class is located at src/main/java/app/Main.java, from project root.

If executed properly you are presented with the main screen of A-Game.

### Trello
Trello invite: https://trello.com/invite/theateam56792781/384e07271977110340c15ce9b62c8ab0

### Buglist:
- If you move in one direction and then suddenly move in another, where you still hold down the key for the original direction, and then let go of the key in the new direction, the player will stop.
- If you put on fullscreen before you press start, in other words, in main menu or in game over menu, the sizes of textures becomes enlarged.
- Sprites/texture placement relative to object collision placement is slightly wrong (scaling issue).

### Licence
(Player.java/Enemy.java, Contact classes) Copyright (C) dermetfan, All rights reserved
URL: https://hg.sr.ht/~dermetfan/tiledmapgame/browse/TiledMapGame/src/net/dermetfan/tiledMapGame/entities/Player.java?rev=tip

Source for backgrounds:
- cave: https://cutewallpaper.org/download.php?file=/21/pixel-cave-background/Pin-by-Steven-Soetanto-on-Pixel-games-in-2019-Pixel-art-.jpg
- clouds: https://www.artstation.com/artwork/GX03kB
- winScreen: https://www.xmple.com/wallpaper/gradient-blue-black-linear-1050x1400-c2-000000-00ffff-a-90-f-14-image/
- menuMusic: https://www.chosic.com/download-audio/29282/
- winScreenMusic: https://www.chosic.com/download-audio/29786/
- gameMusic: https://www.chosic.com/download-audio/31992/
- hurtSound: http://freesoundeffect.net/sound/cartoon-minion-hurt-sound-effect
- last win music: https://www.videvo.net/royalty-free-music-track/ultimate-victory-30s/232836/
