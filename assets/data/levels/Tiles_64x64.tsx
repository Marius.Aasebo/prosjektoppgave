<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.1" name="Tiles_64x64" tilewidth="64" tileheight="64" tilecount="64" columns="8">
 <image source="Tiles_64x64.png" width="512" height="512"/>
 <tile id="0">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="ruby" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="58">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="59">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="61">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
